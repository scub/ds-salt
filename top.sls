base:
  '*':
    - shells
    - python
    - users
    - edit.vim
    - security
  'E@^(celadon|cinnabar|cthulhu|indigo|lavender|saffron|verus).darkscience.net$':
    - inspircd
    - nginx
  'pallet.darchoods.net':
    - inspircd
    - nginx
  'G@os:FreeBSD':
    - security.freebsd
  'G@os:Debian':
    - security.debian
