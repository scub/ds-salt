
{% if grains['os'] == 'Debian' %}
  {% set user = 'irc' %}
{% else %}
  {% set user = 'ircd' %}
ircd_user:
  user.present:
    - name: {{user}}
    - uid: 73
{% endif %}

{{ pillar['path'] }}etc/inspircd:
  file.recurse:
    - template: jinja
    - source: salt://inspircd/inspircd
    - user: {{user}}
    - recurse:
      - user

{{ pillar['path'] }}etc/inspircd/pki/certificate.crt:
  file.managed:
    - makedirs: True
    - contents_pillar: irc-ssl_cert
    - user: {{user}}

{{ pillar['path'] }}etc/inspircd/pki/certificate.key:
  file.managed:
    - contents_pillar: irc-ssl_key
    - user: {{user}}

{{ pillar['path'] }}etc/inspircd/{{ grains['host'] }}.conf:
  file.managed:
    - template: jinja
    - source: salt://inspircd/files/host_config
    - user: {{user}}

{% if grains['os'] == 'FreeBSD' %}
inspircd install:
  ports.installed:
    - name: irc/inspircd
    - options:
      - GNUTLS: True
{% elif grains['os'] == 'Debian' %}
gnutls install:
  pkg.latest:
    - name: gnutls-bin
inspircd install:
  pkg.latest:
    - name: inspircd
init script:
  file.managed:
    - name: /etc/init.d/inspircd
    - source: salt://inspircd/files/sysv-init
systemd unit:
  file.managed:
    - name: /lib/systemd/system/inspircd.service
    - source: salt://inspircd/files/systemd-unit
systemd reload:
  cmd.run:
    - name: systemctl daemon-reload
    - watch:
      - file: /lib/systemd/system/inspircd.service
/usr/lib/inspircd/data:
  file.directory:
    - user: {{user}}
    - group: {{user}}
    - recurse:
      - user
      - group
{% endif %}

/var/log/inspircd:
  file.directory:
    - user: {{user}}
    - group: {{user}}
    - recurse:
      - user
      - group

/var/run/inspircd:
  file.directory:
    - user: {{user}}
    - group: {{user}}
    - recurse:
      - user
      - group


inspircd service:
  service.running:
    - name: inspircd
    - enable: True
    - reload: True
    - watch: 
      - file: {{ pillar['path'] }}etc/inspircd/pki/certificate.crt
      - file: {{ pillar['path'] }}etc/inspircd/pki/certificate.key
      - file: {{ pillar['path'] }}etc/inspircd
