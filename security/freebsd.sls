freebsd-update cron:
  cron.present:
    - user: root
    - minute: 0
    - hour: 2

pf.config:
  file.managed:
    - name: /etc/pf.conf
    - source: salt://security/freebsd/firewall
    - user: root
    - group: wheel
    - mode: 0600

persistence:
  file.append:
    - name: /etc/rc.conf
    - source: salt://security/freebsd/rc.append

pf.service:
  service.running:
    - name: "pf"
    - reload: True
    - enable: True
    - require:
      - file: 'pf.config'
    - watch:
      - file: 'pf.config'
