{{ pillar['path'] }}etc/nginx/nginx.conf:
  file.managed:
    - template: jinja
    - source: salt://nginx/template/nginx.conf

{{ pillar['path'] }}etc/nginx/conf.d:
  file.recurse:
    - template: jinja
    - mkdirs: True
    - source: salt://nginx/template/vhosts

nginx:
  pkg.latest: []
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: {{ pillar['path'] }}etc/nginx/conf.d
      - file: {{ pillar['path'] }}etc/nginx/nginx.conf
      
